package com.srccodes.example.hibernate;

import java.util.List;
import java.util.Properties;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

/**
 * Hello world!
 * 
 */
public class App {
	private static SessionFactory sessionFactory = null;  
	private static ServiceRegistry serviceRegistry = null;  
	
	static{
		configureSessionFactory();
	}
	  
	private static SessionFactory configureSessionFactory() throws HibernateException {  
	    Configuration configuration = new Configuration();  
	    configuration.configure();  
	    
	    Properties properties = configuration.getProperties();
	    
		serviceRegistry = new ServiceRegistryBuilder().applySettings(properties).buildServiceRegistry();          
	    sessionFactory = configuration.buildSessionFactory(serviceRegistry);  
	    
	    return sessionFactory;  
	}
	
	
	public static List<Contact> getContacts() {
		
		Session session = null;
		
		try {
			session = sessionFactory.openSession();
				
			// Fetching saved data
			@SuppressWarnings("unchecked")
			List<Contact> contactList = session.createQuery("from Contact").list();
			
			return contactList;
			
		} catch (Exception ex) {
			ex.printStackTrace();
			
			// Rolling back the changes to make the data consistent in case of any failure 
			// in between multiple database write operations.
			
			return null;
			
		} finally{
			if(session != null) {
				session.close();
			}
		}		
	}
	
	
	public static void createContact(Contact myContact){
		
		Session session = null;
		Transaction tx=null;
		
		try {
			
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			// Saving to the database
			session.save(myContact);
			
			// Committing the change in the database.
			session.flush();
			tx.commit();
			
			
		} catch (Exception ex) {
			ex.printStackTrace();
			
			// Rolling back the changes to make the data consistent in case of any failure 
			// in between multiple database write operations.
			tx.rollback();
		} finally{
			if(session != null) {
				session.close();
			}
		}		
	}
	


}
