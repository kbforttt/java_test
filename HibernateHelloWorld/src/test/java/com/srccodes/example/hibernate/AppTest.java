package com.srccodes.example.hibernate;

import java.io.File;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest extends TestCase {
	public AppTest(String testName) {
		super(testName);
	}

	public static Test suite() {
		return new TestSuite(AppTest.class);
	}

	public void testApp() {
		new File("mydb.db").delete();

		for (int i = 0; i < 5; i++) {
			App.createContact(new Contact(i, "Name " + i, "Address " + i));
		}
		assertTrue(App.getContacts().size() == 5);
	}
	
	public void testFail() {
		assertTrue(false);
	}
}
